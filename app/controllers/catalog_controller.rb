class CatalogController < ApplicationController
  skip_before_action :authenticate_user!, except: [:add_to_cart]
  before_action :product, only: [:show]
  before_action :check_grade, only: [:index, :index2]

  def index
    tabela_vigente = PriceTable.current.first

    @catalog_products = Product.joins(:product_price_tables)
          .where(product_price_tables: { price_table_id: tabela_vigente.try(:id) })
          .select('products.*, product_price_tables.price')
          .order(params[:ordem])
          .page(params[:page])
          .per(params[:grade])
  end

  def index2
    tabela_vigente = PriceTable.current.first

    if tabela_vigente.blank?
      @nome_tabela = 'Não há Tabela Vigente !'
    else
      @nome_tabela = tabela_vigente.name
    end 

    produtos_tabela_vigente = 
    Product.joins("LEFT JOIN product_price_tables on product_price_tables.product_id = products.id")
    .select('products.*, product_price_tables.price')
    produtos_tabela_vigente = produtos_tabela_vigente.page(params[:page])
    @catalog_products = produtos_tabela_vigente.per(params[:grade])
  end

  def show
  end

  private

  def product
    @catalog_product = Product.includes(:product_price_tables).find(params[:id])
  end

  def check_grade
    params[:grade] = 9 if (params[:grade].to_i rescue 0) == 0
  end
end