class OrdersController < ApplicationController
  def meus_pedidos
    @pedidos = current_user.sales_orders.where(status: 'finalizado').page(params[:page]).per(5)
  end 

  def detalhes_pedido
    @detalhes_pedido = current_user.sales_orders.includes(:items).find(params[:sales_order_id])
  end
end
