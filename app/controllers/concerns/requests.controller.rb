class RequestsController < ApplicationController

  def new
    tabela_vigente = PriceTable.current.first

    @catalog_products = Product.joins(:product_price_tables)
          .where(product_price_tables: { price_table_id: tabela_vigente.try(:id) })
          .select('products.*, product_price_tables.price')
          .order("products.name ASC")
          .page(params[:page])
          .per(params[:grade])
  end
end