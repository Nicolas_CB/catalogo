require 'csv'
class Admin::DashboardController < Admin::BaseController

  def pedidos
    @user = User.all
    @pedidos = SalesOrder.where(status: 'finalizado')
    @pedidos = @pedidos.where("user_id = ?", params[:usuario_escolhido]) if params[:usuario_escolhido].present?

    respond_to do |format|
      format.html { @pedidos = @pedidos.page(params[:page]).per(5) }
      format.js { @pedidos = @pedidos.page(params[:page]).per(5) }
      format.csv do
        documento = SalesOrder.gera_pedidos_csv(@pedidos.includes(:user))
        send_data documento[:csv].encode(Encoding::ISO_8859_1), filename: "pedidos_#{documento[:date]}.csv"
      end
    end
  end

  def detalhes_pedido
    @detalhes_pedido = SalesOrder.includes(:items).where(user_id: params[:user_id], id: params[:pedido_id])
    @detalhes_pedido = @detalhes_pedido.first
  end

  def exportar_lista_produtos
    status = params[:status]

    #Caso nenhum parametro seja passado, busca por todos produtos
    if status.blank?
      @produtos = Product.includes(:product_images).all.order('code')
    else
      @produtos = Product.includes(:product_images).where(status: status).order('code')
    end

    respond_to do |format|
      format.csv do 
        documento = Product.exportar_lista_produtos(@produtos)
        send_data documento[:csv].encode(Encoding::ISO_8859_1), filename: "produtos_#{documento[:date]}.csv"
      end
    end
  end

  def importar_produtos
    if !params[:csv].present?
      render :arquivo_faltando
    else
      Product.importar_produtos(params[:csv])
      render :import_realizado
    end
  end
end