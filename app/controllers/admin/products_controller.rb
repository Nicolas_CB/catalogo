class Admin::ProductsController < Admin::BaseController
  before_action :set_product, only: [:show, :edit, :update, :destroy]

  def index
    @products = Product.all.order('name').page(params[:page]).per(9)
    @products = @products.where("name ILIKE ?", "%#{params[:search]}%") if params[:search].present?
  end

  def show
  end

  def new
    @product = Product.new
    @product.product_images.build
  end

  def edit
    puts @product.status
  end

  def create
    @product = Product.new(product_params)
    respond_to do |format|
      if @product.save
        format.html { redirect_to [:admin,@product], notice: 'Product was successfully created.' }
        format.json { render :show, status: :created, location: @product }
      else
        format.html { render :new }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @product.update(product_params)
        format.html { redirect_to [:admin,@product], notice: 'Product was successfully updated.' }
        format.json { render :show, status: :ok, location: @product }
      else
        format.html { render :edit }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @product.destroy
    respond_to do |format|
      format.html { redirect_to admin_products_url, notice: 'Product was successfully destroyed.' }
      format.json { head :no_content }
    end
  end


  private
  def set_product
    @product = Product.find(params[:id])
  end

  def product_params
    if params[:padrao].blank?
      params[:padrao] = true
      correct_index = params[:padrao]
    else
      correct_index = params[:padrao][:image_choice]
    end

    params[:product][:product_images_attributes].each do |k, v|
      params[:product][:product_images_attributes][k][:default_image] = (k == correct_index)
    end

    params.require(:product).permit(:code, :name, :status, 
     product_images_attributes:[:id, :image, :default_image, :_destroy])
  end
end
