class Admin::BaseController < ApplicationController
  skip_before_action :authenticate_user!
  before_action :authenticate_administrator!

  private
  def authenticate_administrator!
    redirect_to admin_login_url unless current_administrator.present?
  end

  def current_administrator
    warden.user(:admin)
  end
  helper_method :current_administrator
end
