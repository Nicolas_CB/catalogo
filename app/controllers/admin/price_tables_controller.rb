class Admin::PriceTablesController < Admin::BaseController
  before_action :set_price_table, only: [:show, :edit, :update, :destroy]

  def index
    @price_tables = PriceTable.all
  end

  def show
    @product_price_tables = @price_table.product_price_tables
    @product_price_tables = @product_price_tables.page(params[:page]).per(2)
  end

  def new
    @price_table = PriceTable.new
  end

  def edit
  end

  def create
    @price_table = PriceTable.new(price_table_params)
    respond_to do |format|
      if @price_table.save
        format.html { redirect_to [:admin,@price_table], notice: 'Price table was successfully created.' }
        format.json { render :show, status: :created, location: @price_table }
      else
        format.html { render :new }
        format.json { render json: @price_table.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @price_table.update(price_table_params)
        format.html { redirect_to [:admin,@price_table], notice: 'Price table was successfully updated.' }
        format.json { render :show, status: :ok, location: @price_table }
      else
        format.html { render :edit }
        format.json { render json: @price_table.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @price_table.destroy
    respond_to do |format|
      format.html { redirect_to admin_price_tables_path, notice: 'Price table was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_price_table
      @price_table = PriceTable.find(params[:id])
    end

    def price_table_params
      params.require(:price_table).permit(:id,:name, :initial_date, :end_date, 
                     product_price_tables:[:id,:price, :product_id, :price_tables_id])
    end
end
