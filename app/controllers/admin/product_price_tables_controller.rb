class Admin::ProductPriceTablesController < Admin::BaseController
  before_action :set_price_table
  before_action :set_product_price_table, only: [:show, :edit, :update, :destroy]

  def index
    @product_price_tables = atualiza_paginacao
    @product_price_tables = @product_price_tables.joins(:product)
             .where("name ILIKE ?", "%#{params[:search]}%") if params[:search].present?
    render :atualiza_tabela
  end

  def show
  end

  def new
    @product_price_table = @price_table.product_price_tables.new
    render :abre_modal
  end

  def edit
    render :abre_modal
  end

  def create
    @product_price_table = @price_table.product_price_tables.new(product_price_table_params)
    if @product_price_table.save
      atualiza_paginacao
      render :fecha_modal
    else
      render :abre_modal
    end
  end

  def update
    if @product_price_table.update(product_price_table_params)
      atualiza_paginacao
      render :fecha_modal
    else
      render :abre_modal
    end
  end

  def destroy
    @product_price_table.destroy
    atualiza_paginacao
    render :atualiza_tabela
  end

  def atualiza_paginacao
    @product_price_tables = @price_table.product_price_tables
    @product_price_tables = @product_price_tables.page(params[:page]).per(2)
    return @product_price_tables
  end

  private
    def set_price_table
      @price_table = PriceTable.find(params[:price_table_id])
    end

    def set_product_price_table
      @product_price_table = @price_table.product_price_tables.find(params[:id])
    end

    def product_price_table_params
      params.fetch(:product_price_table, {}).permit(:product_id, :price)
    end
end
