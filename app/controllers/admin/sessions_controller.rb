class Admin::SessionsController < Admin::BaseController
  skip_before_action :authenticate_administrator!, only: [:destroy,:new, :create]

  def new
  end

  def create
    admin = warden.authenticate(scope: :admin)
    if admin
      redirect_to admin_administrators_path, alert: "Logged in!"
    else
      flash.alert = warden.message if warden.message.present?
      render :new
    end
  end

  def destroy
    warden.logout(:admin)
    redirect_to admin_login_url, alert: "Logged out!"
  end
end
