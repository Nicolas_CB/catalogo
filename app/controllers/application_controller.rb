class ApplicationController < ActionController::Base
  protect_from_forgery
  before_action :authenticate_user!

private

  def authenticate_user!
    redirect_to :login unless user_authenticated?
  end

  def current_user
    warden.user
  end
  helper_method :current_user
  
  def user_authenticated?
    current_user.present?
  end
  helper_method :user_authenticated?

  def warden
    request.env['warden']
  end

  def current_cart
    @cart ||= current_user.cart
  end
  helper_method :current_cart
end
