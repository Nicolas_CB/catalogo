class CartController < ApplicationController
  skip_before_action :authenticate_user!, only: [:add_item]
  include CartHelper

  def meu_carrinho
  end

  def add_item
    if current_user.nil?
      redirect_to login_path, alert: 'Você precisa estar logado para criar um carrinho !'
    else
      tabela_vigente = PriceTable.current.first
      id_produto = params[:product_id]
      valor_produto = ProductPriceTable.joins(:product)
             .where(product_id: id_produto, price_table_id: tabela_vigente.try(:id))
             .first

      if valor_produto.present?
        item = current_cart.items.new
        item.product_id = id_produto
        item.price = valor_produto.price
        item.save
        redirect_to meu_carrinho_path
      else
        redirect_to catalog_path, alert: 'erro'
      end
    end
  end

  def remove_item
    produto = params[:item]
    current_cart.items.destroy(produto)
  end

  def limpa_carrinho
    current_cart.items.destroy_all
  end

  def add_quantidade_item
    id_item = params[:item]
    quantidade = params[:quantidade].to_i
    item = current_cart.items.find(id_item)
    item.amount = quantidade + 1
    item.save
  end

  def remove_quantidade_item
    id_item = params[:item]
    quantidade = params[:quantidade].to_i
    item = current_cart.items.find(id_item)
    item.amount = quantidade - 1
    item.save
  end

  def altera_quantidade_item
    id_item = params[:id_item]
    quantidade = params[:quantidade_digitada].to_i
    item = current_cart.items.find(id_item)
    item.amount = quantidade
    item.save
  end

  def fecha_pedido
    current_cart.notes = params[:nota]
    current_cart.amount = valor_total
    current_cart.status = "finalizado"
    current_cart.save
  end
end