module CartHelper
  def total_itens_carrinho
     current_cart.items.map { |i| i.amount }.reduce(&:+).to_i
  end

  def valor_total
    current_cart.items.map { |i| i.price * i.amount }.reduce(&:+).to_i
  end
end
