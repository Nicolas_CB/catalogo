module ProductsHelper
  #Verifica se o produto possui uma imagem padrão a ser exibida
  def product_has_image(product)
    if product.product_images.blank?
      imagem_padrao = product.product_images.new
      imagem_padrao.image.default_url
    else
      product.default_image.image.thumb
    end
  end

  def permite_add_produto(product)
    if product.pronta_entrega? || product.disponivel?
      return true
    end
  end

  def quantidade_produto(item)
    id = item.product.id
    valor = current_cart.items.group(:product_id).count
    return valor[id]
  end

  def status_options
    Product.statuses.keys.map { |k| [k.humanize, k] }
  end

  def product_status_badge(product)
    return 'badge-primary' if product.pronta_entrega?
    return 'badge-success' if product.disponivel?
    return 'badge-danger' if product.indisponivel?
    return 'badge-warning' if product.esgotado?
    return 'badge-secondary'
  end

  def tipo_exportacao_produtos(status)
    return "1" if status == "pronta_entrega"
    return "2" if status == "disponivel"
    return "3" if status == "indisponivel"
    return "4" if status == "esgotado"
  end
end
