module CatalogHelper
  def grade_options
  [
    ["3 Produtos", 3],
    ["6 Produtos", 6],
    ["9 Produtos", 9],
  ]
  end

  def order_options
  [
    ["Ordem A-Z", 'products.name ASC'],
    ["Ordem Z-A", 'products.name DESC'],
    ["Maior Preço", 'product_price_tables.price DESC'],
    ["Menor Preço", 'product_price_tables.price ASC']
  ]
  end

  def product_status_badge(product)
    return 'badge-primary' if product.pronta_entrega?
    return 'badge-success' if product.disponivel?
    return 'badge-danger' if product.indisponivel?
    return 'badge-warning' if product.esgotado?
    return 'badge-secondary'
  end
end