# == Schema Information
#
# Table name: users
#
#  id              :integer          not null, primary key
#  name            :string           not null
#  password_digest :string           not null
#  email           :string           not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class User < ApplicationRecord
  has_secure_password

  has_many :sales_orders

  validates :name, presence: true
  validates :password_digest, presence: true
  validates :email, presence: true, uniqueness: true

  def cart
    self.sales_orders.carrinho.first_or_create
  end
end
