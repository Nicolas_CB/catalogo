# == Schema Information
#
# Table name: product_images
#
#  id            :integer          not null, primary key
#  image         :string           not null
#  default_image :boolean
#  product_id    :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
# Indexes
#
#  index_product_images_on_product_id  (product_id)
#
# Foreign Keys
#
#  fk_rails_...  (product_id => products.id)
#

class ProductImage < ApplicationRecord
  belongs_to :product

  mount_uploader :image, AvatarUploader

  validates :image, presence: true
end
