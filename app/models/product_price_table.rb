# == Schema Information
#
# Table name: product_price_tables
#
#  id             :integer          not null, primary key
#  price          :decimal(10, 2)   not null
#  product_id     :integer
#  price_table_id :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#
# Indexes
#
#  index_product_price_tables_on_price_table_id  (price_table_id)
#  index_product_price_tables_on_product_id      (product_id)
#
# Foreign Keys
#
#  fk_rails_...  (price_table_id => price_tables.id)
#  fk_rails_...  (product_id => products.id)
#

class ProductPriceTable < ApplicationRecord
  validates :product_id, uniqueness: { scope: [:price_table_id]}
  validates :price, presence: true
  belongs_to :price_table
  belongs_to :product
end
