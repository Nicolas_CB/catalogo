# == Schema Information
#
# Table name: products
#
#  id         :integer          not null, primary key
#  code       :string           not null
#  name       :string           not null
#  deleted    :boolean
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  status     :integer          not null
#

class Product < ApplicationRecord
  after_update_commit :verifica_imagem_padrao
  validates :code, presence: true, uniqueness: true
  validates :name, presence: true
  has_many :product_images
  has_many :product_price_tables
  enum status: { pronta_entrega: 1, disponivel: 2, indisponivel: 3, esgotado: 4 }

  accepts_nested_attributes_for :product_images, allow_destroy: true
  #reject_if: lambda {|attributes| attributes['image'].blank?}

  def default_image
    product_images.where(default_image: true).first
  end

  def self.exportar_lista_produtos(lista)
    date = Time.now
    date.strftime("%H:%M:%S %d-%m-%Y")

    csv = CSV.generate(:col_sep => ';', :headers => true) do |row|
      row << ['Codigo', 'Nome', 'Status', 'Tem Imagem ?', 'Quantidade de Imagens']
      lista.each do |produto|
        row << ["#{produto.code}","#{produto.name}","#{produto.status.humanize}", 
          "#{produto.product_images.exists?}","#{produto.product_images.count}"]
        end
      end
      return {csv: csv, date: date}
    end

   def self.importar_produtos(planilha)
     CSV.foreach(planilha.tempfile,:encoding => Encoding::ISO_8859_1, :headers => true) do |row|
       novo_registro = Product.find_or_initialize_by(code: row['Codigo'])
       novo_registro.name = row['Nome']
       novo_registro.status = row['Status']
       novo_registro.save
     end
   end

end

def verifica_imagem_padrao
  product_images.each do |v|
    if v.default_image == true
      break
    else
      product_images[0].default_image = true
      product_images[0].save
    end
  end
end