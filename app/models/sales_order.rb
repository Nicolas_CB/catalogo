# == Schema Information
#
# Table name: sales_orders
#
#  id         :integer          not null, primary key
#  amount     :decimal(10, 2)   default(0.0), not null
#  status     :integer          not null
#  notes      :string
#  user_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_sales_orders_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#

class SalesOrder < ApplicationRecord
  enum status: { carrinho: 1, finalizado: 2 }

  belongs_to :user

  has_many :items

  def self.gera_pedidos_csv(pedidos)
    date = Time.now
    date.strftime("%H:%M:%S %d-%m-%Y")

    csv = CSV.generate(:col_sep => ';', :headers => true) do |row|
      row << ['ID Pedido','Cliente','Valor do Pedido', 'Data do Pedido']
      pedidos.each do |pedido|
        row << ["#{pedido.id}", "#{pedido.user.name}","#{pedido.amount}","#{pedido.created_at}"]
      end
    end
    return {csv: csv, date: date}
  end
end