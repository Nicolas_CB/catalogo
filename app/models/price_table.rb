# == Schema Information
#
# Table name: price_tables
#
#  id           :integer          not null, primary key
#  name         :string           not null
#  initial_date :date             not null
#  end_date     :date             not null
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class PriceTable < ApplicationRecord
  validates :name, presence: true, uniqueness: true
  validates :initial_date, presence: true
  validates :end_date, presence: true
  validate :verifica_datas

  has_many :product_price_tables, dependent: :destroy
  has_many :products, through: :product_price_tables

  scope :current, -> { where('? BETWEEN initial_date AND end_date', Date.today) }
  
  accepts_nested_attributes_for :product_price_tables, allow_destroy: true

  private

  def verifica_datas
    errors.add(:base,'Data final precisa ser maior que Data Inicio') if initial_date > end_date

    if initial_date.present? and end_date.present?
      sql = <<-SQL
        (? BETWEEN initial_date and end_date)
        OR
        (? BETWEEN initial_date and end_date)
      SQL

      tables = PriceTable.where(sql, initial_date, end_date)
      tables = tables.where('id != ?', self.id) unless self.new_record?

      if tables.any?
        errors.add(:base, 'Já possui uma tabela de preço no período informado')
      end
    end
  end
end
