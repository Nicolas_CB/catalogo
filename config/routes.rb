# == Route Map
#
# You don't have any routes defined!
# 
# Please add some routes in config/routes.rb.
# 
# For more information about routes, see the Rails guide: http://guides.rubyonrails.org/routing.html.
# 

Rails.application.routes.draw do
  scope constraints: lambda { |r| r.env['warden'].user.nil?} do
    get "signup", to: "users#new", as: "signup"
    get "login", to: "sessions#create", as: "login"
  end

  get "logout", to: "sessions#destroy", as: "logout"
  
  root to: "catalog#index"

  resources :users

  resources :sessions

  resources :orders do 
    get 'meus_pedidos', on: :collection
    get 'meus_pedidos/detalhes_pedido/:sales_order_id', to: 'orders#detalhes_pedido', as: 'detalhes_pedido'
  end


  resources :catalog, only: [:index, :show] do 
    get :index2, on: :collection
  end

  get 'meu_carrinho', to: 'cart#meu_carrinho', as: 'meu_carrinho'
  get 'add_quantidade_item', to: 'cart#add_quantidade_item', as: 'add_quantidade_item'
  get 'remove_quantidade_item', to: 'cart#remove_quantidade_item', as: 'remove_quantidade_item'
  get 'altera_quantidade_item', to: 'cart#altera_quantidade_item', as: 'altera_quantidade_item'
  get 'abre_modal', to: 'cart#abre_modal', as: 'abre_modal'

  post 'add_item/:product_id', to: 'cart#add_item', as: 'add_item'
  post 'fecha_pedido', to: 'cart#fecha_pedido', as: 'fecha_pedido'

  delete 'remove_item', to: 'cart#remove_item', as: 'remove_item'
  delete 'limpa_carrinho', to: 'cart#limpa_carrinho', as: 'limpa_carrinho'

  namespace :admin do
    resources :administrators
    resources :products
    resources :sessions
    resources :price_tables do
      resources :product_price_tables  
    end

    get 'login', to: 'sessions#new', as: 'login'
    post 'login', to: 'sessions#create', as: 'authenticate'
    get 'logout', to: 'sessions#destroy', as: 'logout'

    get 'pedidos', to: 'dashboard#pedidos', as: 'pedidos'
    get 'pedidos/detalhes_pedido/:pedido_id', to: 'dashboard#detalhes_pedido', as: 'detalhes_pedido'
    get 'exportar_lista_produtos', to: 'dashboard#exportar_lista_produtos', as: 'exportar_lista_produtos'
    post 'importar_produtos', to: 'dashboard#importar_produtos', as: 'importar_produtos'
  end

end
