Rails.application.config.middleware.use Warden::Manager do |manager|
  manager.default_strategies :password
  manager.default_scope = :user
  manager.failure_app = lambda { |env| SessionsController.action(:new).call(env) }

end

Warden::Manager.serialize_into_session do |object|
  [object.class.name, object.id]
end

Warden::Manager.serialize_from_session do |data|
  klass, id = data
  klass.constantize.find(id)
end

Warden::Strategies.add(:password) do
  def valid?
    params['email'] && params['password']
  end
  
  def authenticate!
    klass = User
    klass = Administrator if scope == :admin
    user = klass.find_by_email(params['email'])
    if user.nil?
      fail "Invalid E-mail"
    elsif user && user.authenticate(params['password'])
      success! user
      puts user
    else
      fail "Invalid Password"
    end
  end
end