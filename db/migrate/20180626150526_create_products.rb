class CreateProducts < ActiveRecord::Migration[5.1]
  def change
    create_table :products do |t|
      t.string :code, null: false
      t.string :name, null: false
      t.boolean :deleted

      t.timestamps
    end
  end
end
