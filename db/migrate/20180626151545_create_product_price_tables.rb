class CreateProductPriceTables < ActiveRecord::Migration[5.1]
  def change
    create_table :product_price_tables do |t|
      t.string :price

      t.references :product, foreign_key: true
      t.references :price_table, foreign_key: true

      t.timestamps
    end
  end
end
