class CreateProductImages < ActiveRecord::Migration[5.1]
  def change
    create_table :product_images do |t|
      t.string :image, null: false
      t.boolean :default_image

      t.references :product, foreign_key: true

      t.timestamps
    end
  end
end
