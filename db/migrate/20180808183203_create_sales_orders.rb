class CreateSalesOrders < ActiveRecord::Migration[5.1]
  def change
    create_table :sales_orders do |t|
      t.decimal :amount, null: false, :precision => 10, :scale => 2, default: 0
      t.integer :status, null: false
      t.string :notes

      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
