class CriaEAlteraAlgunsCamposProduct < ActiveRecord::Migration[5.1]
  def change
    change_column :product_price_tables, :price, :decimal, :precision => 10, :scale => 2, null: false
    add_column :products, :status, :integer, using: 'price::integer', null: false, default: 1
  end
end
