class ArrumaCampoPrice < ActiveRecord::Migration[5.1]
  def change
    change_column :product_price_tables, :price, :integer, using: 'price::integer', null: false
  end
end
