class CreatePriceTables < ActiveRecord::Migration[5.1]
  def change
    create_table :price_tables do |t|
      t.string :name, null: false
      t.date :initial_date, null: false
      t.date :end_date, null: false

      t.timestamps
    end
  end
end
