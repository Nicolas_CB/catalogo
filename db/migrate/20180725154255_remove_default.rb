class RemoveDefault < ActiveRecord::Migration[5.1]
  def change
    change_column_default(:products, :status, nil)
  end
end
