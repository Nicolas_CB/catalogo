class AlteraTipoPrice < ActiveRecord::Migration[5.1]
  def change
    change_column :product_price_tables, :price, :integer, using: 'price::integer'
  end
end
