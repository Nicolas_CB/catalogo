class AlteraTabelaItem < ActiveRecord::Migration[5.1]
  def change
    add_column :items, :amount, :integer, using: 'amount::integer', null: false, default: 1
  end
end
