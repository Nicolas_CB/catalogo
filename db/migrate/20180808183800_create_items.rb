class CreateItems < ActiveRecord::Migration[5.1]
  def change
    create_table :items do |t|
      t.decimal :price, null: false, :precision => 10, :scale => 2

      t.references :sales_order, foreign_key: true
      t.references :product, foreign_key: true

      t.timestamps
    end
  end
end
