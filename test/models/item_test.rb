# == Schema Information
#
# Table name: items
#
#  id             :integer          not null, primary key
#  price          :decimal(10, 2)   not null
#  sales_order_id :integer
#  product_id     :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  amount         :integer          default(1), not null
#
# Indexes
#
#  index_items_on_product_id      (product_id)
#  index_items_on_sales_order_id  (sales_order_id)
#
# Foreign Keys
#
#  fk_rails_...  (product_id => products.id)
#  fk_rails_...  (sales_order_id => sales_orders.id)
#

require 'test_helper'

class ItemTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end
