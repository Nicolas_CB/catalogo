require 'test_helper'

class ProductPriceTablesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @product_price_table = product_price_tables(:one)
  end

  test "should get index" do
    get product_price_tables_url
    assert_response :success
  end

  test "should get new" do
    get new_product_price_table_url
    assert_response :success
  end

  test "should create product_price_table" do
    assert_difference('ProductPriceTable.count') do
      post product_price_tables_url, params: { product_price_table: {  } }
    end

    assert_redirected_to product_price_table_url(ProductPriceTable.last)
  end

  test "should show product_price_table" do
    get product_price_table_url(@product_price_table)
    assert_response :success
  end

  test "should get edit" do
    get edit_product_price_table_url(@product_price_table)
    assert_response :success
  end

  test "should update product_price_table" do
    patch product_price_table_url(@product_price_table), params: { product_price_table: {  } }
    assert_redirected_to product_price_table_url(@product_price_table)
  end

  test "should destroy product_price_table" do
    assert_difference('ProductPriceTable.count', -1) do
      delete product_price_table_url(@product_price_table)
    end

    assert_redirected_to product_price_tables_url
  end
end
