require 'test_helper'

class PriceTablesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @price_table = price_tables(:one)
  end

  test "should get index" do
    get price_tables_url
    assert_response :success
  end

  test "should get new" do
    get new_price_table_url
    assert_response :success
  end

  test "should create price_table" do
    assert_difference('PriceTable.count') do
      post price_tables_url, params: { price_table: {  } }
    end

    assert_redirected_to price_table_url(PriceTable.last)
  end

  test "should show price_table" do
    get price_table_url(@price_table)
    assert_response :success
  end

  test "should get edit" do
    get edit_price_table_url(@price_table)
    assert_response :success
  end

  test "should update price_table" do
    patch price_table_url(@price_table), params: { price_table: {  } }
    assert_redirected_to price_table_url(@price_table)
  end

  test "should destroy price_table" do
    assert_difference('PriceTable.count', -1) do
      delete price_table_url(@price_table)
    end

    assert_redirected_to price_tables_url
  end
end
